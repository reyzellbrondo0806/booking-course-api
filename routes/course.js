//[Activity - s45]

//Dependencies
const express = require("express");
const courseController = require("../controllers/course")
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

//Routing Component
const router = express.Router();

//Routes
router.post("/", verify, verifyAdmin, courseController.addCourse);

//[End of Activity]

// route for retrieving all courses
router.get("/all", courseController.getAllCourses)

// route for retrieving all active courses
router.get("/", courseController.getAllActiveCourses)

// route for retrieving course
router.get("/:courseId", courseController.getCourse)

// update specific course
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse)

router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse)

router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse)

//Export Route System
module.exports = router
